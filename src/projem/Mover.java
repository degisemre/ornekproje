package projem;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Mover extends Thread {

    private final int sensitivity = 8;

    private Top myBall;
    private Thread t;

    private int speedX;
    private int speedY;

    
    Mover(Top ball)
    {
        myBall = ball;

        speedX  = 0;
        speedY  = 0;

        myBall.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent me) {
                kicked(me.getX(),me.getY());
            }
        });
    }
    
    
    public void start () {

        if (t == null) {
            t = new Thread(this, "Mover");
         t.start ();
      }
    }
      
    @Override
    public void run() {
        
        long i = 10;
        while( true )
       {
            try {
                sleep(i);
            } catch (InterruptedException ex) {
                System.err.println("Hata!...");
            }
            move();
            myBall.reloadLocation();
       }        
    }
    
    
    private void moveX()
    {
        if(speedX > 0)
        {
            if(myBall.getPosX() + myBall.cap + speedX < Pencerem.width)
            {
                myBall.setPosX(myBall.getPosX() + speedX);
            }
            else
            {
                speedX *= -1;
                Pencerem.puan.updatePuan(false);
            }
        }
        else
        {
            if(myBall.getPosX() + speedX > 0)
            {
                myBall.setPosX(myBall.getPosX() + speedX);
            }    
            else
            {
                Pencerem.puan.updatePuan(false);
                speedX *= -1;
            }
        }
    }
    
    private void moveY()
    {
        if(speedY > 0)
        {
            if(myBall.getPosY() + myBall.cap + speedY < Pencerem.height)
            {
                myBall.setPosY(myBall.getPosY() + speedY);
            }
            else
            {
                Pencerem.puan.updatePuan(false);
                speedY *= -1;
            }
        }
        else
        {
            if(myBall.getPosY() + speedY > 0)
            {
                myBall.setPosY(myBall.getPosY() + speedY);
            }
            else
            {
                Pencerem.puan.updatePuan(false);
                speedY *= -1;
            }
        }
    }
    private void move()
    {
        moveX();
        moveY();
    }
    
    public void kicked(int clickX, int clickY) 
    {
        if(clickX < myBall.cap/2)
            speedX = (myBall.cap/2)/sensitivity - (clickX / sensitivity);
        else if(clickX > myBall.cap/2)
        {
            clickX-=myBall.cap/2;
            speedX = (clickX / sensitivity) * -1 ;
                if(speedX > 0 )
                    speedX *= -1;
        }
        else
            speedX = 0;
        
        if(clickY < myBall.cap/2)
            speedY = ((myBall.cap/2) - clickY)/ sensitivity;
        else if(clickY > myBall.cap/2)
        {
            clickY-=myBall.cap/2;
            speedY = (clickY / sensitivity) * -1 ;
                if(speedY > 0 )
                    speedY *= -1;
        }
        else
            speedY = 0;
        
        Pencerem.puan.updatePuan(true);
                
    }
    
}
