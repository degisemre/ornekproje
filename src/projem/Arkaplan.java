package projem;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Arkaplan extends JLabel{

    Arkaplan()
    {
        setBounds(0, 0, Pencerem.width,Pencerem.height);
    }
    
    @Override
    public void paint(Graphics kalem) {
        super.paint(kalem); 

        Image resim = new ImageIcon("img/background.jpg").getImage();
        kalem.drawImage(resim, 0, 0, null);
    }
    
    
}
