package projem;

import java.awt.Color;
import java.awt.Graphics;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class PuanTablosu extends JLabel implements Runnable{

    private int puan;
    private int maxPuan;
    private Timer zaman;
    private int posX;
    private int posY;
    private int genislik;
    private int yukseklik;
    private int saniye;

    PuanTablosu()
    {
        this.puan       = 0;
        this.maxPuan    = 0;
        this.saniye     = 0;
        this.posX       = 3*(Pencerem.width/4);
        this.posY       = 0;
        this.genislik   = Pencerem.width / 4;
        this.yukseklik  = 60;
        
        setFont (getFont ().deriveFont ((float)(yukseklik / 2 - 10)));
        setBounds(posX,posY,genislik,yukseklik-5);
        
    }
    
    public void start()
    {
       zaman.start();
    }
    
    public void stop()
    {
        zaman.stop();
    }
    
    public void reset()
    {
        this.puan = 0;
        this.saniye = 0;
    }   
    
    @Override
    public void paint(Graphics kalem) {
        super.paint(kalem); //To change body of generated methods, choose Tools | Templates.
        kalem.setColor(Color.GREEN);
        kalem.fillRect(0, 0, genislik, yukseklik);
        
        if(puan == 0)
            kalem.setColor(Color.red);
        else
            kalem.setColor(Color.black);
        
        kalem.drawString("Puan = " + puan  , 0 , yukseklik / 3);
        kalem.setColor(Color.black);
        kalem.drawString("Süre = " + saniye, 0 , (yukseklik / 3) * 2 + 10);
    }

    @Override
    public void run() {
        while(true)
        {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(PuanTablosu.class.getName()).log(Level.SEVERE, null, ex);
        }

        saniye++;
        repaint();
        }
    }
    
    public void updatePuan(boolean type)
    {
        if(puan > maxPuan)
            maxPuan = puan;

        if(type==true)
            puan+=10;
        else if(puan != 0)
        {
            JOptionPane.showMessageDialog(null, "Oyun Bitti Şimdiye kadar ki Max Puan = " + maxPuan,"",JOptionPane.ERROR_MESSAGE );
            puan    = 0;
        }
        
        repaint();
    }
    
    
    
}
