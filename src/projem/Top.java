package projem;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Top extends JLabel {

    public static final int cap = 80;
    
    private int sayac = 0;
    private int posX;
    private int posY;
    private Mover mover;

    Top()
    {
        posX = 0;
        posY = 0;
        setIcon( new ImageIcon("img/ball.png") );
        setBounds(posX, posY, cap,cap);

        mover = new Mover(this);
        mover.start();
    }
    
    public int getPosX()
    {
        return this.posX;
    }
    
    public int getPosY()
    {
        return this.posY;
    }
    
    public void setPosX(int posX)
    {
        if(posX > 0 && posX < Pencerem.width)
            this.posX = posX;
    }
    
    public void setPosY(int posY)
    {
        if(posY > 0 && posY < Pencerem.height)
            this.posY = posY;
    }
    
    public void reloadLocation()
    {
        setLocation(posX,posY);
        repaint();        
    }
}
